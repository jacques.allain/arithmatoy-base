# Aucun n'import ne doit être fait dans ce fichier


def nombre_entier(n: int) -> str:
    return f"{'S' * n}0"
    pass


def S(n: str) -> str:
    return f"S{n}"
    pass


def addition(a: str, b: str) -> str:
    if a == "0" :
        return b
    return S(addition(a[1:], b))
    pass

def multiplication(a: str, b: str) -> str:
    if a == "0" :
        return "0"
    return addition(multiplication(a[1:], b), b)
    pass

def facto_ite(n: int) -> int:
    result = 1
    for i in range(1, n + 1):
        result *= i
    return result
    pass

def facto_rec(n: int) -> int:
    if n == 0:
        return 1
    return n * facto_rec(n - 1)
    pass

def fibo_rec(n: int) -> int:
    if n <= 1:
        return n
    return fibo_rec(n - 1) + fibo_rec(n - 2)
    pass

def fibo_ite(n: int) -> int:
    if n <= 1:
        return n
    a, b = 0, 1
    for _ in range(n - 1):
        a, b = b, a + b
    return b
    pass

def golden_phi(n: int) -> float:
    return fibo_ite(n + 1) / fibo_ite(n)
    pass

def sqrt5(n: int) -> int:
    if n == 0:
        return 0
    return 1 + sqrt5(n-1) / 2
    pass

def pow(a: float, n: int) -> float:
    if n == 0:
        return 1.0
    elif n < 0:
        return 1 / pow(a, -n)
    elif n % 2 == 0:
        half_pow = pow(a, n // 2)
        return half_pow * half_pow
    else:
        return a * pow(a, n - 1)
    pass
